# View dwarf preferences and current stocks, filtered by trader resources
# by disacorns
# FIXME: fixme's sprinkled around here.

=begin
trade prefs
===========
View a list of preferences for all dwarves in a fortress, sorted by the trade
agreement page, optionally filtered by a specific trade civilizations resources

Usage: ``trade_prefs <civ_name>``.
The ``civ_name`` is optional, and can be ``none``. Preferred items will
be filtered by resources of the named civilization.
``civ_name`` can be part of a name and is evaluated as the first match:
  e.g. a civilization named "cog tathtak" could be referenced by a
  civ_name "cog".
If ``civ_name`` is ``none``, then show all preferences.
If no civ name is provided:
  If a caravan is present, then use the civilization of the caravan.
  If no caravan is present will be evaluated as ``none``.

=end
# wtf is a feather tree egg??!
class ItemCommon
  def self.find_flags(item)
    case item.mode
    when :Creature
      if item.material
        item.material.flags
      else
        item.creature.flags
      end
    else
      item.material.flags
    end
  end

  # FIXME: I dont know how else to get the flag keys
  def self.flags_parsed(flags)
    flags.inspect.to_s.tr('<>', '').split(' ')[1..-1]
  end

  def self.item_flags(item)
    flags = find_flags(item)
    flags_parsed(flags)
  end
end

# civilizations that can trade
class TradeEntity
  attr_reader :ent_items, :trader_name, :civ
  def initialize; end

  def civ_by_id(id)
    @civ = df.world.entities.all[id]
  end

  def civ_by_name(civ_name)
    civs = trade_entities
    @civ = civs.find { |civ| civ.name.to_s.include?(civ_name) }
  end

  # find entities that trade
  # TODO: figure out how to find civs that actually trade with fort
  def trade_entities
    # FIXME: probably missing elves
    positions = %w[GUILD_REPRESENTATIVE OUTPOST_LIAISON]
    df.world.entities.all.select do |ent|
      ent.type == :Civilization &&
        !ent.positions.own.select { |pos| positions.include?(pos.code) }.empty?
    end
  end

  # some resources have mat_type & mat_index
  def cat_add(cat, cat_name)
    cat.mat_type.each_with_index do |_m, indx|
      add_resource(cat.mat_type[indx], cat.mat_index[indx], indx, cat_name)
    end
  end

  # some just have a mat_index
  def cat_add_provide_type(cat, mat_type, cat_name)
    cat.to_a.each_with_index do |mat_index, indx|
      add_resource(mat_type, mat_index, indx, cat_name)
    end
  end

  def add_resource(mat_type, mat_index, indx, cat_name)
    # mat = DFHack::MaterialInfo.new(mat_type, mat_index)
    @ent_items[[mat_type, mat_index]] ||= {}
    @ent_items[[mat_type, mat_index]][cat_name] = indx
  end

  # FIXME: this is gross
  def items
    @ent_items = {}
    ent = @civ
    @trader_name = ent.name

    # leather
    cat_add(ent.resources.organic.leather, 'Leather')
    # clothplant
    cat_add(ent.resources.organic.fiber, 'Thread(Plant)')
    # clothsilk
    cat_add(ent.resources.organic.silk, 'Thread(Silk)')
    # crafts
    cat_add(ent.resources.misc_mat.crafts, 'Crafts')
    # wood
    cat_add(ent.resources.organic.wood, 'Wood')
    # metalbars
    cat_add_provide_type(ent.resources.metals.to_a, 0, 'MetalBars')
    # gems
    cat_add_provide_type(ent.resources.gems, 0, 'Gems')
    # stoneblocks
    cat_add_provide_type(ent.resources.stones, 0, 'Stones')
    # seeds. see plants
    # anvils
    # weapons
    # trainingweapons
    # ammo
    # trapcomponents
    # diggingimplements
    # bodywear
    # headwear
    # handwear
    # footwear
    # shields
    # toys
    # instruments
    # pets
    cat_add_provide_type(ent.resources.animals.pet_races, -1, 'Pets')
    # drinks
    cat_add(ent.resources.misc_mat.booze, 'Drinks')
    # cheese
    cat_add(ent.resources.misc_mat.cheese, 'Cheese')
    # powders
    cat_add(ent.resources.misc_mat.powders, 'Powders')
    # extracts
    # FIXME: the kangaroo's milk listed here shows up as KangarooMan Milk?
    # gross on so many levels
    cat_add(ent.resources.misc_mat.extracts, 'Extracts')
    # meat
    cat_add(ent.resources.misc_mat.meat, 'Meat')
    cat_add_provide_type(ent.resources.egg_races, -1, 'Meat')
    # fish
    cat_add_provide_type(ent.resources.fish_races, 31, 'Fish')
    # plants
    # FIXME: Im Guessing the mat_type here
    (419..426).each do |mat_type|
      cat_add_provide_type(ent.resources.plants.mat_index, mat_type, 'Plants')
      cat_add_provide_type(ent.resources.tree_fruit_growths, mat_type, 'GardenVegetables')
      cat_add_provide_type(ent.resources.tree_fruit_plants, mat_type, 'GardenVegetables')
      cat_add_provide_type(ent.resources.shrub_fruit_growths, mat_type, 'GardenVegetables')
      cat_add_provide_type(ent.resources.shrub_fruit_plants, mat_type, 'GardenVegetables')
    end

    # fruitsnuts. see plants
    # gardenvegetables. see plants
    # meatfishrecipes ???
    # otherrecipes. ????
    # stone. see stoneblocks
    # cages
    cat_add(ent.resources.misc_mat.cages, 'Cages')

    # bagsleather
    # bagsplant
    # threadplant. see cloth plant
    # threadsilk. see clothsilk
    # ropesplant
    # ropessilk
    # barrels
    cat_add(ent.resources.misc_mat.barrels, 'Barrels')
    # flaskswaterskinsbarrels
    cat_add(ent.resources.misc_mat.flasks, 'Flasks')
    # quivers
    cat_add(ent.resources.misc_mat.quivers, 'Quivers')
    # backpacks
    cat_add(ent.resources.misc_mat.backpacks, 'Backpacks')
    # sand
    cat_add(ent.resources.misc_mat.sand, 'Sand')
    # glass
    cat_add(ent.resources.misc_mat.glass, 'Glass')
    # misc
    cat_add(ent.resources.wood_products.material, 'Misc')
    # buckets
    # splints
    # crutches
    # eggs
    # bagsyarn
    # ropesyarn
    # clothyarn
    # threadyarn
    cat_add(ent.resources.organic.wool, 'Thread(Yarn)')
    # tools
    # clay
  end
end

# compile the prefences for dwarves in fortress
# therapist says rope reed paper where dfhack says thread ???
class DwarfPrefs
  attr_reader :items
  def initialize
    # keys are items, values are flags
    @items = {}
    dwarf_prefs(df.unit_citizens)
  end

  # FIXME This is gross
  # determine which Trade Category is relevant for each item
  def compile_trade_flags(item)
    # @items[item] ||= []
    flags = ItemCommon.item_flags(item)
    iflags = []
    iflags.push('Leather') if flags.include?('LEATHER')
    iflags.push('Wood') if flags.include?('WOOD')
    iflags.push('MetalBars') if flags.include?('IS_METAL')
    iflags.push('Gems') if flags.include?('IS_GEM')
    iflags.push('Seeds') if flags.include?('SEED_MAT')
    if flags.include?('ALCOHOL') || flags.include?('ALCOHOL_CREATURE')
      iflags.push('Drinks')
    end
    iflags.push('Cheese') if flags.include?('CHEESE')
    if flags.include?('NO_STONE_STOCKPILE') || flags.include?('POWDER_MISC')
      iflags.push('Powders')
    end
    iflags.push('Extracts') if flags.include?('LIQUID_MISC_CREATURE')
    if (flags.include?('MEAT') && !item.to_s.include?('SEAHORSE')) || (flags.include?('VERMIN_GROUNDER') && !flags.include?('CASTE_MUST_BREATHE_WATER')) || flags.include?('VERMIN_SOIL') || flags.include?('LARGE_ROAMING')
      iflags.push('Meat')
    end
    if flags.include?('VERMIN_FISH') || (flags.include?('VERMIN_GROUNDER') && flags.include?('CASTE_MUST_BREATHE_WATER')) || flags.include?('MEAT') && item.to_s.include?('SEAHORSE')
      iflags.push('Fish')
    end
    if flags.include?('STRUCTURAL_PLANT_MAT') && !item.to_s.include?('BERRIES')
      iflags.push('Plants')
    end
    if flags.include?('LEAF_MAT') || flags.include?('STRUCTURAL_PLANT_MAT') && item.to_s.include?('BERRIES')
      iflags.push('GardenVegetables')
    end
    iflags.push('Stones') if flags.include?('IS_STONE')
    iflags.push('Thread(Plant)') if flags.include?('THREAD_PLANT')
    iflags.push('Glass') if flags.include?('IS_GLASS')
    iflags.push('Thread(Yarn)') if flags.include?('YARN')
    if flags.include?('HORN') || flags.include?('BONE') || flags.include?('SHELL') || flags.include?('TOOTH')
      iflags.push('Crafts')
    end

    # puts [item.mat_type, item.mat_index].to_s
    @items[[item.mat_type, item.mat_index]] = iflags
    item
  end

  def find_item_by_pref(pref)
    item ||= DFHack::MaterialInfo.new(pref.mattype, pref.matindex)
    if item.material.nil?
      item.material = df.world.raws.creatures.all[pref.mattype]
    end
    item
  end

  def dwarf_prefs(dwarves)
    pref_types = [:LikeMaterial, :LikeFood]
    prefs = dwarves.map(&:status).map(&:current_soul).map(&:preferences)
    prefs = prefs.map(&:to_a).flatten
    prefs = prefs.select { |pref| pref_types.include?(pref.type) }
    items = prefs.map { |pref| find_item_by_pref(pref) }
    items.each { |item| compile_trade_flags(item) }
  end
end

# show prefs by trade category
class TradePrefs
  def initialize(civ = nil)
    @stocks = Stocks.new
    @items = DwarfPrefs.new.items
    @trader_items = civ.ent_items if civ
    @shown_items = []
    show_flags
    show_unknowns
  end

  def show_flags
    trade_flags = %w[Leather Wood MetalBars Gems Seeds Pets Drinks
                     Cheese Powders Extracts Meat
                     Fish Plants GardenVegetables Stones Thread(Plant)
                     Eggs
                     Glass Thread(Yarn) Crafts]
    trade_flags.each { |flag| show_flag(flag) }
  end

  def show_flag(flag)
    puts "\n\t#{flag}"
    items = @items.keys.select { |item| @items[item].include?(flag) }
    items = items.sort_by { |item| trader_index(item, flag) } if @trader_items
    items.each do |item|
      @shown_items.push(item)
      show_item(item, flag)
    end
  end

  def trader_index(item, flag)
    return nil unless @trader_items
    t_item = @trader_items[item]
    if t_item && t_item[flag]
      t_item[flag]
    elsif t_item
      1000
    else
      2000
    end
  end

  def show_t_index(item, flag)
    t_index = trader_index(item, flag)
    case t_index
    when 1000 then'Not Matched'
    when 2000 then 'Not Found'
    else
      "#{t_index / 17}, #{t_index % 17}"
    end
  end

  def show_item(item, flag)
    t_index = @trader_items ? show_t_index(item, flag) : ''
    name = readable_name(item, flag).capitalize.ljust(30)
    puts "#{name}#{t_index.ljust(11)}#{@stocks.item_count(item)}"
    # useful for sorting out sorting problems
    # puts "#{item.mat_type}, #{item.mat_index}"
  end

  def find_item_by_a(item_a)
    item = DFHack::MaterialInfo.new(item_a[0], item_a[1])
    if item.material.nil?
      item.material = df.world.raws.creatures.all[item_a[1]]
    end
    item
  end


  # FIXME this is gross, must be a better way
  # FIXME meat shows a very specific preference as in mole kidney
  # while therapist shows just mole
  # don't know if it matters
  # wanted a method that did need a flag, for other uses, too bad
  def readable_name(item, flag)
    return 'no item' unless item
    item = find_item_by_a(item)
    return item.material.name[0] if item.material.class == DFHack::CreatureRaw
    material = if item.material.heat.melting_point <= 10_000
                 item.material.state_name[1]
               else
                 item.material.state_name[0]
               end
    case item.mode
    when :Creature
      return material if %w[Cheese Extracts].include?(flag)
      creature = "#{item.creature.name[0]} #{material}"
      return creature.gsub('tissue', '')
    when :Inorganic
      return "#{material} bars" if flag == 'MetalBars'
      return material
    when :Builtin
      return material
    end
    material += ' logs' if flag == 'Wood'
    material += ' thread' if flag == 'Thread(Plant)'
    material += ' yarn' if flag == 'Thread(Yarn)'
    return item.plant.name if material == 'plant'
    return item.plant.seed_singular if material == 'seed'
    mat = item.to_s.split(':')[-1]
    growth = item.plant.growths.find { |gr| gr.str_growth_item.include?(mat) }
    return growth.name_plural if growth
    material
  end

  def show_unknowns
    puts "\n\tUNKNOWN"
    (@items.keys - @shown_items).each do |item|
      show_item(item, nil)
      puts item.map(&:to_s).join(', ')
      puts ItemCommon.item_flags(find_item_by_a(item)).join(', ') if item # && item.material
      puts "\n"
    end
  end
end

# find items in fortress stocks
class Stocks
  def initialize
    @items_h = {}
    @items_h.default = 0
    # caching is much faster then trying to count the items later
    # still slow though
    all = df.world.items.all.to_a

    # seems slightly faster if selects first
    all = mat_select(all)

    # FIXME: thread returns all items made of that thread
    all.each do |item|
      @items_h[[item.mat_type, item.mat_index]] ||= 0
      @items_h[[item.mat_type, item.mat_index]] += 1
    end
  end

  # select materials in fortress.
  # im sure im not doing this right
  # and its slooow
  def mat_select(items)
    items.select { |item| item.flags.in_inventory && item.respond_to?('mat_type')}
  end

  def item_count(item)
    @items_h[[item[0], item[1]]]
  end
end

def print_help
  puts 'Usage: ``trade_prefs <civ_name>``.'
  puts 'The ``civ_name`` is optional, and can be ``none``. Preferred items will'
  puts 'be filtered by resources of the named civilization.'
  puts '``civ_name`` can be part of a name and is evaluated as the first match:'
  puts '  e.g. civ_name "cog t" will match a civilization named "cog tathtak"'
  puts 'If ``civ_name`` is ``none``, then show all preferences.'
  puts 'If no civ name is provided:'
  puts '  If a caravan is present, then use the civilization of the caravan.'
  puts '  If no caravan is present will be evaluated as ``none``.'
  throw :script_finished
end

def diplomat_civ(diplomat)
  entity_links = diplomat.hist_figure_tg.entity_links
  member_link = entity_links.find do |lnk|
    lnk.class == DFHack::HistfigEntityLinkMemberst
  end
  trade_entity = TradeEntity.new
  trade_entity.civ_by_id(member_link.entity_id)
  trade_entity
end

def find_civ(civ_name)
  trade_entity = TradeEntity.new
  trade_entity.civ_by_name(civ_name)
  trade_entity
end

print_help if $script_args && $script_args.any?{ |arg| %w[help ? -?].include?(arg) }
civ_name = $script_args[0..-1].join(' ') if $script_args.length > 0

if civ_name.nil?
  diplomat = df.world.units.all.find { |unit| unit.flags1.diplomat }
  civ = diplomat_civ(diplomat) if diplomat
end
civ = find_civ(civ_name) if civ_name && civ_name != 'none'
civ.items if civ.civ
TradePrefs.new(civ)
if civ.civ
  puts "Showing results for: #{civ.civ.name}, id: #{civ.civ.id}"
else
  puts 'No Trader'
end
puts 'Possible civilization names:'
puts TradeEntity.new.trade_entities.map(&:name).map(&:to_s).sort.join(', ')
